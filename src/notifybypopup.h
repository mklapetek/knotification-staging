/*
   Copyright (C) 2005-2006 by Olivier Goffart <ogoffart at kde.org>
   Copyright (C) 2008 by Dmitry Suzdalev <dimsuz@gmail.com>


   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 */

#ifndef NOTIFYBYPOPUP_H
#define NOTIFYBYPOPUP_H

#include "knotifyplugin.h"
#include <QMap>
#include <QHash>
#include <QStringList>
#include <QXmlStreamEntityResolver>

#include <QDBusError>
#include <QDBusMessage>

class KNotification;
class KPassivePopup;
class QDBusPendingCallWatcher;

class NotifyByPopup : public KNotifyPlugin
{
    Q_OBJECT
public:
    NotifyByPopup(QObject *parent = 0l);
    virtual ~NotifyByPopup();

    virtual QString optionName() { return "Popup"; }
    virtual void notify(KNotification *notification, KNotifyConfig *notifyConfig);
    virtual void close(KNotification *notification);
    virtual void update(KNotification *notification, KNotifyConfig *config);

    void queryPopupServerCapabilities();

private:
    QMap<int, KPassivePopup * > m_popups;
    // the y coordinate of the next position popup should appears
    int m_nextPosition;
    int m_animationTimer;
    void fillPopup(KPassivePopup *, int id, KNotifyConfig *config);
    /**
     * Make sure a popup is completely supported by the notification backend.
     * Changes the popup to be compatible if needed.
     * @param notification the notification data to check
     */
    void ensurePopupCompatibility(KNotification *notification);
    /**
     * Removes HTML from a given string. Replaces line breaks with \n and
     * HTML entities by their 'normal forms'.
     * @param string the HTML to remove.
     * @return the cleaned string.
     */
    QString stripHtml(const QString &text);
    /**
     * Sends notification to DBus "org.freedesktop.notifications" interface.
     * @param id knotify-sid identifier of notification
     * @param replacesId knotify-side notification identifier. If not 0, will
     * request DBus service to replace existing notification with data in config
     * @param config notification data
     * @return true for success or false if there was an error.
     */
    bool sendNotificationToGalagoServer(KNotification *notification, uint replacesId, KNotifyConfig *config);
    /**
     * Sends request to close Notification with id to DBus "org.freedesktop.notifications" interface
     *  @param id knotify-side notification ID to close
     */
    void closeGalagoNotification(KNotification *notification);
    /**
     * Specifies if DBus Notifications interface exists on session bus
     */
    bool m_dbusServiceExists;
    /**
     * DBus notification daemon capabilities cache.
     * Do not use this variable. Use #popupServerCapabilities() instead.
     * @see popupServerCapabilities
     */
    QStringList m_popupServerCapabilities;

    QList<QPair<KNotification*, KNotifyConfig*> > m_notificationQueue;
    /**
     * Whether the DBus notification daemon capability cache is up-to-date.
     */
    bool m_dbusServiceCapCacheDirty;
    /**
     * Find the caption and the icon name of the application
     */
    void getAppCaptionAndIconName(KNotifyConfig *config, QString *appCaption, QString *iconName);

protected:
    void timerEvent(QTimerEvent *event);

private Q_SLOTS:
    void slotPopupDestroyed();
    void slotLinkClicked(const QString & );
    // slot to catch appearance or dissapearance of Notifications DBus service
    void slotServiceOwnerChanged(const QString &, const QString &, const QString &);
    // slot which gets called when DBus signals that some notification action was invoked
    void onGalagoNotificationActionInvoked(uint notificationId, const QString &actionKey);
    // slot which gets called when DBus signals that some notification was closed
    void onGalagoNotificationClosed(uint, uint);

    void onGalagoServerReply(QDBusPendingCallWatcher *callWatcher);

    void onGalagoServerCapabilitiesReceived(const QStringList &capabilities);

private:
    /**
        * Maps knotify notification IDs to DBus notifications IDs
        */
    QHash<int,uint> m_idMap;

    /*
        * As we communicate with the notification server over dbus
        * we use only ids, this is for fast KNotifications lookup
        */
    QHash<uint, KNotification*> m_notifications;

    /**
        * A class for resolving HTML entities in XML documents (used
        * during HTML stripping)
        */
    class HtmlEntityResolver : public QXmlStreamEntityResolver
    {
        QString resolveUndeclaredEntity( const QString &name );
    };
};

#endif
