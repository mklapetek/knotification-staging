/*
   Copyright (C) 2005-2009 by Olivier Goffart <ogoffart at kde.org>
   Copyright (C) 2008 by Dmitry Suzdalev <dimsuz@gmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 */

#include "notifybypopup.h"
// #include "imageconverter.h"
// #include "notifybypopupgrowl.h"

// #include <kdebug.h>
#include <kpassivepopup.h>
// #include <kiconloader.h>
// #include <kdialog.h>
// #include <khbox.h>
// #include <kvbox.h>
// #include <kcharsets.h>
#include "knotifyconfig.h"
#include "knotification.h"

#include <QBuffer>
#include <QImage>
#include <QLabel>
#include <QTextDocument>
#include <QApplication>
#include <QDesktopWidget>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusServiceWatcher>
#include <QXmlStreamReader>
#include <QDebug>

#include <kconfiggroup.h>

static const char dbusServiceName[] = "org.freedesktop.Notifications";
static const char dbusInterfaceName[] = "org.freedesktop.Notifications";
static const char dbusPath[] = "/org/freedesktop/Notifications";

NotifyByPopup::NotifyByPopup(QObject *parent) 
  : KNotifyPlugin(parent),
    m_animationTimer(0),
    m_dbusServiceExists(false),
    m_dbusServiceCapCacheDirty(true)
{
    QRect screen = QApplication::desktop()->availableGeometry();
    m_nextPosition = screen.top();

    // check if service already exists on plugin instantiation
    QDBusConnectionInterface *interface = QDBusConnection::sessionBus().interface();
    m_dbusServiceExists = interface && interface->isServiceRegistered(dbusServiceName);

    if (m_dbusServiceExists) {
        slotServiceOwnerChanged(dbusServiceName, QString(), "_"); //connect signals
    }

    // to catch register/unregister events from service in runtime
    QDBusServiceWatcher *watcher = new QDBusServiceWatcher(this);
    watcher->setConnection(QDBusConnection::sessionBus());
    watcher->setWatchMode(QDBusServiceWatcher::WatchForOwnerChange);
    watcher->addWatchedService(dbusServiceName);
    connect(watcher, SIGNAL(serviceOwnerChanged(QString,QString,QString)),
            SLOT(slotServiceOwnerChanged(QString,QString,QString)));

    if (!m_dbusServiceExists) {
        bool startfdo = false;
#ifdef Q_WS_WIN
        startfdo = true;
#else
        if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
            QDBusMessage message = QDBusMessage::createMethodCall("org.freedesktop.DBus",
                                                                  "/org/freedesktop/DBus",
                                                                  "org.freedesktop.DBus",
                                                                  "ListActivatableNames");

            // FIXME - async
            QDBusReply<QStringList> reply = QDBusConnection::sessionBus().call(message);
            if (reply.isValid() && reply.value().contains(dbusServiceName)) {
                startfdo = true;
                // We need to set m_dbusServiceExists to true because dbus might be too slow
                // starting the service and the first call to NotifyByPopup::notify
                // might not have had the service up, by setting this to true we
                // guarantee it will still go through dbus and dbus will do the correct
                // thing and wait for the service to go up
                m_dbusServiceExists = true;
            }
        }
#endif
        if (startfdo) {
            QDBusConnection::sessionBus().interface()->startService(dbusServiceName);
        }
    }
}


NotifyByPopup::~NotifyByPopup()
{
    Q_FOREACH (KPassivePopup *p, m_popups) {
        p->deleteLater();
    }
}

void NotifyByPopup::notify(KNotification *notification, KNotifyConfig *notifyConfig)
{
    qDebug() << "Got notification with id" << notification->id();
    qDebug() << "Current active notifications:" << m_popups.keys() << m_notifications.keys();

    if (m_popups.contains(notification->id()) || m_notifications.contains(notification->id())) {
        qDebug() << "the popup is already shown";
        finish(notification);
        return;
    }

    // if Notifications DBus service exists on bus,
    // it'll be used instead
    if (m_dbusServiceExists) {
        if (m_dbusServiceCapCacheDirty) {
            qDebug() << "No caps yet, queuing...";
            m_notificationQueue.append(qMakePair(notification, notifyConfig));
            qDebug() << "Currently" << m_notificationQueue.size() << "notifications queued";
            queryPopupServerCapabilities();
        } else {
            if (!sendNotificationToGalagoServer(notification, 0, notifyConfig)) {
                finish(notification); //an error ocurred.
            }
        }
        return;
    }

    // Default to 6 seconds if no timeout has been defined
//     int timeout = notifyConfig->timeout == -1 ? 6000 : notifyConfig->timeout;

    qDebug() << "No DBus service available, doing nothing";

    // if Growl can display our popups, use that instead
//     if (NotifyByPopupGrowl::canPopup()) {
//         KNotifyConfig *c = ensurePopupCompatibility(notifyConfig);
//
//         QString appCaption, iconName;
//         getAppCaptionAndIconName(c, &appCaption, &iconName);
//
//         KIconLoader iconLoader(iconName);
//         QPixmap appIcon = iconLoader.loadIcon(iconName, KIconLoader::Small);
//
//         NotifyByPopupGrowl::popup(&appIcon, timeout, appCaption, c->text);
//
//         // Finish immediately, because current NotifyByPopupGrowl can't callback
//         finish(id);
//         delete c;
//         return;
//     }
//
//     KPassivePopup *pop = new KPassivePopup(notifyConfig->winId);
//     m_popups[id] = pop;
//     fillPopup(pop, id, notifyConfig);
//
//     QRect screen = QApplication::desktop()->availableGeometry();
//     pop->setAutoDelete(true);
//     connect(pop, SIGNAL(destroyed()), this, SLOT(slotPopupDestroyed()));
//
//     pop->setTimeout(timeout);
//     pop->adjustSize();
//     pop->show(QPoint(screen.left() + screen.width()/2 - pop->width()/2 , m_nextPosition));
//     m_nextPosition += pop->height();
}

void NotifyByPopup::slotPopupDestroyed()
{
    const QObject *destroyedPopup = sender();
    if (!destroyedPopup) {
        return;
    }

    QMap<int, KPassivePopup*>::iterator it;
    for (it = m_popups.begin(); it != m_popups.end(); ++it) {
        QObject *popup = it.value();
        if (popup && popup == destroyedPopup) {
            //FIXME
//             finish(it.key());
            m_popups.remove(it.key());
            break;
        }
    }

    //relocate popup
    if (!m_animationTimer) {
        m_animationTimer = startTimer(10);
    }
}

void NotifyByPopup::timerEvent(QTimerEvent *event)
{
    if (event->timerId() != m_animationTimer) {
        return KNotifyPlugin::timerEvent(event);
    }

    bool cont = false;
    QRect screen = QApplication::desktop()->availableGeometry();
    m_nextPosition = screen.top();

    Q_FOREACH (KPassivePopup *popup, m_popups)
    {
        int y = popup->pos().y();
        if (y > m_nextPosition) {
            y = qMax(y - 5, m_nextPosition);
            m_nextPosition = y + popup->height();
            cont = cont || y != m_nextPosition;
            popup->move(popup->pos().x(), y);
        } else {
            m_nextPosition += popup->height();
        }
    }

    if (!cont) {
        killTimer(m_animationTimer);
        m_animationTimer = 0;
    }
}

void NotifyByPopup::slotLinkClicked(const QString &adr)
{
    unsigned int id = adr.section('/' , 0 , 0).toUInt();
    unsigned int action = adr.section('/' , 1 , 1).toUInt();

//    qDebug() << id << " " << action;

    if (id == 0 || action == 0) {
        return;
    }

    emit actionInvoked(id, action);
}

void NotifyByPopup::close(KNotification *notification)
{
    delete m_popups.take(notification->id());

    if (m_dbusServiceExists) {
        closeGalagoNotification(notification);
    }
}

void NotifyByPopup::update(KNotification *notification, KNotifyConfig *notifyConfig)
{
//     if (m_popups.contains(id)) {
//         KPassivePopup *p = m_popups[id];
//         fillPopup(p, id, notifyConfig);
//         return;
//     }

    // if Notifications DBus service exists on bus,
    // it'll be used instead
    if (m_dbusServiceExists) {
        sendNotificationToGalagoServer(notification, notification->id(), notifyConfig);
        return;
    }

    // otherwise, just display a new Growl notification
//     if (NotifyByPopupGrowl::canPopup()) {
//         notify(id, notifyConfig);
//     }
}

void NotifyByPopup::fillPopup(KPassivePopup *popup, int id, KNotifyConfig *notifyConfig)
{
//     QString appCaption;
//     QString iconName;
//     getAppCaptionAndIconName(notifyConfig, &appCaption, &iconName);
//
//     KIconLoader iconLoader(iconName);
//     QPixmap appIcon = iconLoader.loadIcon(iconName, KIconLoader::Small);
//
//     KVBox *vb = popup->standardView(notifyConfig->title.isEmpty() ? appCaption : notifyConfig->title,
//                                     notifyConfig->image.isNull() ? notifyConfig->text : QString(),
//                                     appIcon);
//
//     if (!notifyConfig->image.isNull()) {
//         QPixmap pix = QPixmap::fromImage(notifyConfig->image.toImage());
//         KHBox *hb = new KHBox(vb);
//         hb->setSpacing(KDialog::spacingHint());
//
//         QLabel *pil = new QLabel(hb);
//         pil->setPixmap(pix);
//         pil->setScaledContents(true);
//
//         if (pix.height() > 80 && pix.height() > pix.width()) {
//             pil->setMaximumHeight(80);
//             pil->setMaximumWidth(80 * pix.width() / pix.height());
//         } else if(pix.width() > 80 && pix.height() <= pix.width()) {
//             pil->setMaximumWidth(80);
//             pil->setMaximumHeight(80*pix.height()/pix.width());
//         }
//
//         KVBox *vb2 = new KVBox(hb);
//         QLabel *msg = new QLabel(notifyConfig->text, vb2);
//         msg->setAlignment(Qt::AlignLeft);
//     }
//
//
//     if ( !notifyConfig->actions.isEmpty() )
//     {
//         QString linkCode=QString::fromLatin1("<p align=\"right\">");
//         int i=0;
//         foreach ( const QString & it , notifyConfig->actions )
//         {
//             i++;
//             linkCode+=QString::fromLatin1("&nbsp;<a href=\"%1/%2\">%3</a> ").arg( id ).arg( i ).arg( Qt::escape(it) );
//         }
//         linkCode+=QString::fromLatin1("</p>");
//         QLabel *link = new QLabel(linkCode , vb );
//         link->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
//         link->setOpenExternalLinks(false);
//         //link->setAlignment( AlignRight );
//         QObject::connect(link, SIGNAL(linkActivated(const QString &)), this, SLOT(slotLinkClicked(const QString& ) ) );
//         QObject::connect(link, SIGNAL(linkActivated(const QString &)), popup, SLOT(hide()));
//     }
//
//     popup->setView( vb );
}

void NotifyByPopup::slotServiceOwnerChanged(const QString &serviceName, const QString &oldOwner, const QString &newOwner)
{
    qDebug() << serviceName << oldOwner << newOwner;
    // tell KNotify that all existing notifications which it sent
    // to DBus had been closed
    Q_FOREACH (KNotification *n, m_notifications.values()) {
        finished(n);
    }
    m_notifications.clear();

    m_dbusServiceCapCacheDirty = true;
    m_popupServerCapabilities.clear();
    m_notificationQueue.clear();

    if (newOwner.isEmpty()) {
        m_dbusServiceExists = false;
    } else if (oldOwner.isEmpty()) {
        m_dbusServiceExists = true;

        // connect to action invocation signals
        bool connected = QDBusConnection::sessionBus().connect(QString(), // from any service
                                                               dbusPath,
                                                               dbusInterfaceName,
                                                               "ActionInvoked",
                                                               this,
                                                               SLOT(onGalagoNotificationActionInvoked(uint,QString)));
        if (!connected) {
            qWarning() << "warning: failed to connect to ActionInvoked dbus signal";
        }

        connected = QDBusConnection::sessionBus().connect(QString(), // from any service
                                                          dbusPath,
                                                          dbusInterfaceName,
                                                          "NotificationClosed",
                                                          this,
                                                          SLOT(onGalagoNotificationClosed(uint,uint)));
        if (!connected) {
            qWarning() << "warning: failed to connect to NotificationClosed dbus signal";
        }
    }
}


void NotifyByPopup::onGalagoNotificationActionInvoked(uint notificationId, const QString &actionKey)
{
    // find out knotify id
    KNotification *n = m_notifications[notificationId];
    if (n == 0) {
        qDebug() << "failed to find KNotification id for dbus_id" << notificationId;
        return;
    }
    qDebug() << "action" << actionKey << "invoked for notification " << notificationId;

    // FIXME: ...whyyy??
    // emulate link clicking
    slotLinkClicked(QString("%1/%2").arg(n->id()).arg(actionKey));
    // now close notification - similar to popup behaviour
    // (popups are hidden after link activation - see 'connects' of linkActivated signal above)
    closeGalagoNotification(n);
}

void NotifyByPopup::onGalagoNotificationClosed(uint dbus_id, uint reason)
{
//     Q_UNUSED(reason)
    // find out knotify id
    KNotification *n = m_notifications[dbus_id];
    if (n == 0) {
        qDebug() << "failed to find KNotification for dbus_id" << dbus_id;
        return;
    }
    qDebug() << "Notification" << n->id() << "(" << dbus_id << ") closed because:" << reason;
    m_notifications.remove(dbus_id);
    finished(n);
}

void NotifyByPopup::getAppCaptionAndIconName(KNotifyConfig *notifyConfig, QString *appCaption, QString *iconName)
{
    KConfigGroup globalgroup(&(*notifyConfig->eventsfile), QString("Global"));
    *appCaption = globalgroup.readEntry("Name", globalgroup.readEntry("Comment", notifyConfig->appname));

    KConfigGroup eventGroup(&(*notifyConfig->eventsfile), QString("Event/%1").arg(notifyConfig->eventid));
    if (eventGroup.hasKey("IconName")) {
        *iconName = eventGroup.readEntry("IconName", notifyConfig->appname);
    } else {
        *iconName = globalgroup.readEntry("IconName", notifyConfig->appname);
    }
}

bool NotifyByPopup::sendNotificationToGalagoServer(KNotification *notification, uint replacesId, KNotifyConfig *notifyConfig_nocheck)
{
    KNotification *replaceNotification;
    // If we were passed a replace ID, we need to find what we're replacing
    if (replacesId != 0) {
        replaceNotification = m_notifications.value(replacesId);
        // If we found nothing, we do nothing
        //FIXME: shouldn't the notification meant as a replacement be shown anyway?
        if (!replaceNotification) {
            return false;
        }
    }

    QDBusMessage dbusNotificationMessage = QDBusMessage::createMethodCall(dbusServiceName, dbusPath, dbusInterfaceName, "Notify");

    QList<QVariant> args;

    QString appCaption;
    QString iconName;
    getAppCaptionAndIconName(notifyConfig_nocheck, &appCaption, &iconName);

    // FIXME: rename this to something better reflecting what this is doing...maybe
    ensurePopupCompatibility(notification);

    args.append(appCaption); // app_name
    args.append(replacesId); // replaces_id
    args.append(iconName); // app_icon
    args.append(notification->title().isEmpty() ? appCaption : notification->title()); // summary
    args.append(notification->text()); // body
    // galago spec defines action list to be list like
    // (act_id1, action1, act_id2, action2, ...)
    //
    // assign id's to actions like it's done in fillPopup() method
    // (i.e. starting from 1)
    QStringList actionList;
    int actId = 0;
    Q_FOREACH (const QString &actionName, notification->actions()) {
        actId++;
        actionList.append(QString::number(actId));
        actionList.append(actionName);
    }

    args.append(actionList); // actions

    QVariantMap hintsMap;
    // Add the application name to the hints.
    // According to fdo spec, the app_name is supposed to be the applicaton's "pretty name"
    // but in some places it's handy to know the application name itself
    if (!notification->appName().isEmpty()) {
        hintsMap["x-kde-appname"] = notification->appName();
    }

    //FIXME - reenable/fix
    // let's see if we've got an image, and store the image in the hints map
//     if (!notifyConfig->image.isNull()) {
//         QImage image = notifyConfig->image.toImage();
//         hintsMap["image_data"] = ImageConverter::variantForImage(image);
//     }

    args.append(hintsMap); // hints

    int timeout = notification->flags() & KNotification::CloseOnTimeout ? 6000 : -1;
    args.append(timeout); // expire timout

    dbusNotificationMessage.setArguments(args);

    QDBusPendingCall notificationCall = QDBusConnection::sessionBus().asyncCall(dbusNotificationMessage, -1);

    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(notificationCall, this);
    watcher->setProperty("notificationObject", QVariant::fromValue<KNotification*>(notification));

    connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
            this, SLOT(onGalagoServerReply(QDBusPendingCallWatcher*)));

    return true;

//
//        //FIXME #if 1
//        #if 0
//             int oldId = m_idMap.key(dbus_id, 0);
//             if (oldId != 0) {
// //                 qWarning() << "Received twice the same id "<< dbus_id << "( previous notification: " << oldId << ")";
//                 m_idMap.remove(oldId);
//                 //FIXME
// //                 finish(oldId);
//             }
// #endif
//             m_notifications.insert(dbus_id, notification);
//             qDebug() << "mapping knotify id to dbus id:"<< notification->id() << "=>" << dbus_id;
//
//             return true;
//         } else {
//             qDebug() << "error: received reply with no arguments";
//         }
//     } else if (replyMsg.type() == QDBusMessage::ErrorMessage) {
//         qDebug() << "error: failed to send dbus message";
//     } else {
//         qDebug() << "unexpected reply type";
//     }
//
//     return false;
}

void NotifyByPopup::onGalagoServerReply(QDBusPendingCallWatcher *watcher)
{
    KNotification *notification = watcher->property("notificationObject").value<KNotification*>();
    if (!notification) {
        qWarning() << "Invalid notification object passed in DBus reply watcher";
        return;
    }

    QDBusPendingReply<uint> reply = *watcher;
//     notification->slotReceivedId(reply.argumentAt<0>());

    m_notifications.insert(reply.argumentAt<0>(), notification);
    watcher->deleteLater();
}

void NotifyByPopup::closeGalagoNotification(KNotification *notification)
{
    if (notification->id() == 0) {
        qDebug() << "not found dbus id to close" << notification->id();
        return;
    }

    QDBusMessage m = QDBusMessage::createMethodCall(dbusServiceName, dbusPath,
                                                    dbusInterfaceName, "CloseNotification");
    QList<QVariant> args;
    args.append(notification->id());
    m.setArguments(args);

    // send(..) does not block
    bool queued = QDBusConnection::sessionBus().send(m);

    if (!queued) {
        qWarning() << "Failed to queue dbus message for closing a notification";
    }

}

void NotifyByPopup::queryPopupServerCapabilities()
{
    if (!m_dbusServiceExists) {
//         if (NotifyByPopupGrowl::canPopup()) {
//             return NotifyByPopupGrowl::capabilities();
//         } else {
//             // Return capabilities of the KPassivePopup implementation
//             return QStringList() << "actions" << "body" << "body-hyperlinks"
//                                  << "body-markup" << "icon-static";
//         }
    }

    if (m_dbusServiceCapCacheDirty) {
        QDBusMessage m = QDBusMessage::createMethodCall(dbusServiceName,
                                                        dbusPath,
                                                        dbusInterfaceName,
                                                        "GetCapabilities");

        QDBusConnection::sessionBus().callWithCallback(m,
                                                       this,
                                                       SLOT(onGalagoServerCapabilitiesReceived(QStringList)),
                                                       0,
                                                       -1);
    }
}

void NotifyByPopup::onGalagoServerCapabilitiesReceived(const QStringList &capabilities)
{
    m_popupServerCapabilities = capabilities;
    m_dbusServiceCapCacheDirty = false;

    qDebug() << "Got caps:" << capabilities;
    qDebug() << "Running notify on" << m_notificationQueue.size() << "notifications";

    for (int i = 0; i < m_notificationQueue.size(); i++) {
        notify(m_notificationQueue.at(i).first, m_notificationQueue.at(i).second);
    }

    m_notificationQueue.clear();
}

void NotifyByPopup::ensurePopupCompatibility(KNotification *notification)
{
    if (m_dbusServiceCapCacheDirty) {
        qDebug() << "### Fuck.";
    }

    if (!m_popupServerCapabilities.contains("actions")) {
        notification->setActions(QStringList());
    }

    if (!m_popupServerCapabilities.contains("body-markup")) {
        if (notification->title().startsWith("<html>")) {
            notification->setTitle(stripHtml(notification->title()));
        }
        if (notification->text().startsWith("<html>")) {
            notification->setText(stripHtml(notification->text()));
        }
    }
}

QString NotifyByPopup::stripHtml(const QString &text)
{
    QXmlStreamReader r("<elem>" + text + "</elem>");
    HtmlEntityResolver resolver;
    r.setEntityResolver(&resolver);
    QString result;
    while (!r.atEnd()) {
        r.readNext();
        if (r.tokenType() == QXmlStreamReader::Characters) {
            result.append(r.text());
        } else if (r.tokenType() == QXmlStreamReader::StartElement && r.name() == "br") {
            result.append("\n");
        }
    }

    if (r.hasError()) {
        // XML error in the given text, just return the original string
        qWarning() << "Notification to send to backend which does "
                         "not support HTML, contains invalid XML:"
                      << r.errorString() << "line" << r.lineNumber()
                      << "col" << r.columnNumber();
        return text;
    }

    return result;
}

QString NotifyByPopup::HtmlEntityResolver::resolveUndeclaredEntity(const QString &name)
{
//     QString result = QXmlStreamEntityResolver::resolveUndeclaredEntity(name);
//
//     if (!result.isEmpty()) {
//         return result;
//     }
//
//     QChar ent = KCharsets::fromEntity('&' + name);
//
//     if (ent.isNull()) {
//         qWarning() << "Notification to send to backend which does "
//                          "not support HTML, contains invalid entity: "
//                       << name;
//         ent = ' ';
//     }
//
//     return QString(ent);
    return name;
}

#include "notifybypopup.moc"
