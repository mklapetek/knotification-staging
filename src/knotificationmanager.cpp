/* This file is part of the KDE libraries
   Copyright (C) 2005 Olivier Goffart <ogoffart at kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "knotificationmanager_p.h"
#include "knotification.h"

#include <QDebug>
#include <QHash>
#include <QWidget>
#include <QDBusConnection>
#include <QPointer>

#include <kservicetypetrader.h>

#include "knotify_interface.h"
#include "knotifyplugin.h"
#include "notifybypopup.h"
#include "knotifyconfig.h"

typedef QHash<QString, QString> Dict;

struct KNotificationManager::Private {
    QHash<int, KNotification *> notifications;
//     org::kde::KNotify *knotify;
    QHash<QString, KNotifyPlugin *> notifyPlugins;

    // incremental ids for notifications
    int notifyIdCounter;
};

class KNotificationManagerSingleton
{
public:
    KNotificationManager instance;
};

Q_GLOBAL_STATIC(KNotificationManagerSingleton, s_self)

KNotificationManager *KNotificationManager::self()
{
    return &s_self()->instance;
}

KNotificationManager::KNotificationManager()
    : d(new Private)
{
    qDebug() << "Constructing";
    d->notifyIdCounter = 0;
    qDeleteAll(d->notifyPlugins);
    d->notifyPlugins.clear();
    addPlugin(new NotifyByPopup(this));
//     addPlugin(new NotifyBySound(this));
//     addPlugin(new NotifyByExecute(this));
//     addPlugin(new NotifyByLogfile(this));
    //TODO reactivate on Mac/Win when KWindowSystem::demandAttention will implemented on this system.
    #ifndef Q_WS_MAC
//     addPlugin(new NotifyByTaskbar(this));
    #endif
//     addPlugin(new NotifyByKTTS(this));

    KService::List offers = KServiceTypeTrader::self()->query("KNotify/NotifyMethod");

    QVariantList args;
    QString error;

    Q_FOREACH (const KService::Ptr service, offers) {
        KNotifyPlugin *plugin = service->createInstance<KNotifyPlugin>(this, args, &error);
        if (plugin) {
            addPlugin(plugin);
        } else {
            qDebug() << "Could not load plugin" << service->name() << "due to:" << error;
        }
    }
}

KNotificationManager::~KNotificationManager()
{
//     delete d->knotify;
    delete d;
}

void KNotificationManager::addPlugin(KNotifyPlugin *notifyPlugin)
{
    d->notifyPlugins[notifyPlugin->optionName()] = notifyPlugin;
    connect(notifyPlugin, SIGNAL(finished(KNotification*)), this, SLOT(notifyPluginFinished(KNotification*)));
    connect(notifyPlugin, SIGNAL(actionInvoked(int, int)), this, SLOT(notificationActivated(int, int)));
}

void KNotificationManager::notifyPluginFinished(KNotification *notification)
{
    if (!d->notifications.contains(notification->id())) {
        qDebug() << "Cannot find" << notification->id();
        qDebug() << "Available ids" << d->notifications.keys();
        return;
    }

    notification->deref();


//     Event *e=m_notifications[id];
//     e->ref--;
//     if(e->ref==0)
//         closeNotification( id );

//     if(!m_notifications.contains(id))
//         return;
//     Event *e=m_notifications[id];
//
//     kDebug() << e->id << " ref=" << e->ref;
//
//     //this has to be called before  plugin->close or we will get double deletion because of slotPluginFinished
//     m_notifications.remove(id);
//
//     if(e->ref>0)
//     {
//         e->ref++;
//         KNotifyPlugin *plugin;
//         foreach(plugin , m_plugins)
//         {
//             plugin->close( id );
//         }
//     }
//     notificationClosed(id);
//     delete e;
}

void KNotificationManager::notificationActivated(int id, int action)
{
    if (d->notifications.contains(id)) {
        qDebug() << id << " " << action;
        KNotification *n = d->notifications[id];
        d->notifications.remove(id);
        n->activate(action);
    }
}

void KNotificationManager::notificationClosed(KNotification *notification)
{
    if (d->notifications.contains(notification->id())) {
        qDebug() << notification->id();
        KNotification *n = d->notifications[notification->id()];
        d->notifications.remove(notification->id());
        n->close();
    }
}

void KNotificationManager::close(int id, bool force)
{
    if (force || d->notifications.contains(id)) {
        KNotification *n = d->notifications.take(id);
        qDebug() << "Closing notification" << id;

        Q_FOREACH (KNotifyPlugin *plugin, d->notifyPlugins) {
            plugin->close(n);
        }

        delete n;
    }
}

bool KNotificationManager::notify(KNotification *n, const QPixmap &pix,
                                  const QStringList &actions,
                                  const KNotification::ContextList &contextList,
                                  const QString &appname)
{
    WId winId = n->widget() ? n->widget()->topLevelWidget()->winId()  : 0;

    QByteArray pixmapData;
    {
        QBuffer buffer(&pixmapData);
        buffer.open(QIODevice::WriteOnly);
        pix.save(&buffer, "PNG");
    }

    QVariantList contextVariantList;
    typedef QPair<QString, QString> ContextPair;
    foreach (const ContextPair &context, contextList) {
        QVariantList vl;
        vl << context.first << context.second;
        contextVariantList << vl;
    }

//     QString presentstring=e->config.readEntry("Action");
//     QStringList presents=presentstring.split ('|');
//
//     if (!e->config.contexts.isEmpty() && !presents.first().isEmpty())
//     {
        //Check whether the present actions are absolute, relative or invalid
//     bool relative = presents.first().startsWith('+') || presents.first().startsWith('-');
//     bool valid = true;
//
//     Q_FOREACH (const QString &presentAction, presents) {
//         valid &= ((presentAction.startsWith('+') || presentAction.startsWith('-')) == relative);
//     }
//
//     if (!valid)
//     {
//         kDebug() << "Context " << e->config.contexts << "present actions are invalid! Fallback to default present actions";
//         Event defaultEvent = Event(e->config.appname, ContextList(), e->config.eventid);
//         QString defaultPresentstring=defaultEvent.config.readEntry("Action");
//         presents = defaultPresentstring.split ('|');
//     } else if (relative)
//     {
//         // Obtain the list of present actions without context
//         Event noContextEvent = Event(e->config.appname, ContextList(), e->config.eventid);
//         QString noContextPresentstring = noContextEvent.config.readEntry("Action");
//         QSet<QString> noContextPresents = noContextPresentstring.split ('|').toSet();
//         foreach (const QString & presentAction, presents)
//         {
//             if (presentAction.startsWith('+'))
//                 noContextPresents << presentAction.mid(1);
//             else
//                 noContextPresents.remove(presentAction.mid(1));
//         }
//         presents = noContextPresents.toList();
//         }
//     }
//

    //FIXME: leaking
    KNotifyConfig *notifyConfig = new KNotifyConfig(appname, contextList, n->eventId());

    QString notifyActions = notifyConfig->readEntry("Action");
    qDebug() << "Got notification with actions:" << notifyActions;

    d->notifications.insert(++d->notifyIdCounter, n);
    n->slotReceivedId(d->notifyIdCounter);

    Q_FOREACH (const QString &action, notifyActions.split('|')) {
        if (!d->notifyPlugins.contains(action)) {
            qDebug() << "No plugin for action" << action;
            continue;
        }

        KNotifyPlugin *notifyPlugin = d->notifyPlugins[action];
        n->ref();
        qDebug() << "calling notify on" << notifyPlugin->optionName();
        notifyPlugin->notify(n, notifyConfig);
    }

    // Persistent     => 0  == infinite timeout
    // CloseOnTimeout => -1 == let the server decide
//     int timeout = (n->flags() & KNotification::Persistent) ? 0 : -1;
//
//     QList<QVariant>  args;
//     args << n->eventId() << (appname.isEmpty() ? QCoreApplication::instance()->applicationName() : appname);
//     args.append(QVariant(contextList));
//     args << n->title() << n->text() <<  pixmapData << QVariant(actions) << timeout << qlonglong(winId);
//     return d->knotify->callWithCallback("event", args, n, SLOT(slotReceivedId(int)), SLOT(slotReceivedIdError(QDBusError)));

    //FIXME why?
    return false;
}

void KNotificationManager::update(KNotification *n)
{
    QByteArray pixmapData;
    if (!n->pixmap().isNull()) {
        QBuffer buffer(&pixmapData);
        buffer.open(QIODevice::WriteOnly);
        n->pixmap().save(&buffer, "PNG");
    }

    KNotifyConfig notifyConfig(n->appName(), n->contexts(), n->eventId());

    Q_FOREACH (KNotifyPlugin *p, d->notifyPlugins) {
        p->update(n, &notifyConfig);
    }
}

void KNotificationManager::reemit(KNotification *n, int id)
{
    QVariantList contextList;
    typedef QPair<QString, QString> Context;
    foreach (const Context &ctx, n->contexts()) {
//      qDebug() << "add context " << ctx.first << "-" << ctx.second;
        QVariantList vl;
        vl << ctx.first << ctx.second;
        contextList << vl;
    }

    //FIXME
//     d->knotify->reemit(id, contextList);
}

#include "moc_knotificationmanager_p.cpp"
