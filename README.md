# KNotification

Desktop notifications

## Introduction

KNotification is used to notify the user of an event. It covers feedback and
persistent events.

## Links

- Home page: <https://projects.kde.org/projects/frameworks/knotification>
- Mailing list: <https://mail.kde.org/mailman/listinfo/kde-frameworks-devel>
- IRC channel: #kde-devel on Freenode
- Git repository: <https://projects.kde.org/projects/frameworks/knotifications/repository>
